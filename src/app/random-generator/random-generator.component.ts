import { Component, OnInit } from '@angular/core';
import { LambdaService } from '../services/lambda.service';

@Component({
  selector: 'app-random-generator',
  templateUrl: './random-generator.component.html',
  styleUrls: ['./random-generator.component.scss']
})
export class RandomGeneratorComponent implements OnInit {
  inputno: any ; 
  randomValueGlob: any= [];
  constructor(private ls: LambdaService) {}

  ngOnInit(): void {
  }
  lambda_fxn(){
    console.log(this.inputno)
    let randomvalues = [];
    if (this.inputno !== undefined ) {
        this.ls.getRandomValues(this.inputno).subscribe((data)=>{
          this.randomValueGlob=data
          console.log(data)
        },
        (error)=> console.error(error))
    }
    //console.log(response)
  }
}

