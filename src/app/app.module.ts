import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RandomGeneratorComponent } from './random-generator/random-generator.component';
import { LambdaService } from './services/lambda.service';

@NgModule({
  declarations: [
    AppComponent,
    RandomGeneratorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    LambdaService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
