import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LambdaService {

  constructor(private http:HttpClient) { }
  getRandomValues(n: String){
   return this.http.get("https://adeb3ccbtj.execute-api.us-east-2.amazonaws.com/default/lambda_fxn?n=" + n)

  }
}
